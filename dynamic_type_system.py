def add(a,b):   ##have not defined the type of the objects
    return a+b

print(add(1, 2))        # we can add int. floats strings and list, this is called dynamic typing
print(add(3.4, 6.6))
print(add('add','this'))
print(add([1,2],[3,4]))


print(add("the anser is" , 40))  #this will give an error because the implicit conversion is not defined .
# So strings cant be added to int or float